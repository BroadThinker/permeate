<?php

if (!file_exists("../install/install.lock")) {
    header("location:../install/step1.php");
}
require_once "../core/common.php";

includeAction("$model", "$action");
